import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"


const initialState = {
    languageData: [],
    isLoading: true,
    language: "all"
}

export const getlanguageData = createAsyncThunk('language/getlanguageData', (languages) => {
    return fetch(`https://api.github.com/search/repositories?q=stars:%3E1+language:${languages}&sort=stars&order=desc&type=Repositories`)
        .then((res) => res.json())
        .catch((err) => console.log(err))
})

const languageSlice = createSlice({
    name: 'language',
    initialState,
    reducers: {
        all: (state) => {
            state.language = "all"
        },
        javascript: (state) => {
            state.language = "javascript"
        },
        ruby: (state) => {
            state.language = "ruby"
        },
        java: (state) => {
            state.language = "java"
        },
        css: (state) => {
            state.language = "css"
        },
        python: (state) => {
            state.language = "python"
        },
    },
    extraReducers: {
        [getlanguageData.pending]: (state) => {
            state.isLoading = true
        },
        [getlanguageData.fulfilled]: (state, action) => {
            state.isLoading = false
            state.languageData = action.payload.items
        },
        [getlanguageData.rejected]: (state) => {
            state.isLoading = false
        },
    }
})

export const { all, python, javascript, ruby, java, css } = languageSlice.actions

export default languageSlice.reducer