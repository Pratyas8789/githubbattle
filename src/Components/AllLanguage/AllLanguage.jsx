import React from 'react'
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { useDispatch, useSelector } from 'react-redux';
import { javascript, python, ruby, java, css, all } from '../../LangageData/LanguageSlice'

export default function AllLanguage() {
    const dispatch = useDispatch()
    const { language } = useSelector((store) => store.language)
    const { mode } = useSelector((store) => store.mode)
    return (
        <div className='d-flex justify-content-center' style={{ width: "80%" }}  >
            <ButtonGroup aria-label="Basic example">
                <Button style={{ border: "none", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}`, color: `${language === "all" ? "#BB2E1F" : `${mode === "dark" ? "black" : "white"}`}` }} onClick={() => dispatch(all())} variant="light"><h3>All</h3></Button>
                <Button style={{ border: "none", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}`, color: `${language === "javascript" ? "#BB2E1F" : `${mode === "dark" ? "black" : "white"}`}` }} onClick={() => dispatch(javascript())} variant="light"><h3>JavaScript</h3></Button>
                <Button style={{ border: "none", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}`, color: `${language === "ruby" ? "#BB2E1F" : `${mode === "dark" ? "black" : "white"}`}` }} onClick={() => dispatch(ruby())} variant="light"><h3>Ruby</h3></Button>
                <Button style={{ border: "none", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}`, color: `${language === "java" ? "#BB2E1F" : `${mode === "dark" ? "black" : "white"}`}` }} onClick={() => dispatch(java())} variant="light"><h3>Java</h3></Button>
                <Button style={{ border: "none", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}`, color: `${language === "css" ? "#BB2E1F" : `${mode === "dark" ? "black" : "white"}`}` }} onClick={() => dispatch(css())} variant="light"><h3>CSS</h3></Button>
                <Button style={{ border: "none", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}`, color: `${language === "python" ? "#BB2E1F" : `${mode === "dark" ? "black" : "white"}`}` }} onClick={() => dispatch(python())} variant="light"><h3>Python</h3></Button>
            </ButtonGroup>
        </div>
    )
}
