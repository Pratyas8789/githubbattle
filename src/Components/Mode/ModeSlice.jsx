import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    mode: "dark"
}

const modeSlice = createSlice({
    name: "mode",
    initialState,
    reducers: {
        dark: (state) => {
            state.mode = "light"
        },
        light: (state) => {
            state.mode = "dark"
        }
    }
})

export const { dark, light } = modeSlice.actions

export default modeSlice.reducer