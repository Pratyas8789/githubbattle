import React from 'react'
import Card from 'react-bootstrap/Card';
import { useSelector } from 'react-redux';

import { BsFillPersonFill } from "react-icons/bs";

export default function DisplayCard() {
    const { languageData } = useSelector((store) => store.language)
    const { mode } = useSelector((store) => store.mode)

    let allCards

    if (languageData !== "" && languageData !== undefined) {
        allCards = languageData.map((allinfo, index) => {
            return (
                <Card key={allinfo.id} className='m-2 ' style={{ minWidth: '18rem', maxWidth: "18rem", backgroundColor: `${mode === "dark" ? "#EBEBEB" : "#24282A"}` }}>
                    <div className='d-flex flex-column align-items-center' >
                        <h1 style={{ color: `${mode === "dark" ? "black" : "white"}` }} ># {index + 1}</h1>
                        <Card.Img variant="top" style={{ width: "50%", backgroundColor: "#EBEBEB" }} src={allinfo.owner.avatar_url} />
                        <Card.Body>
                            <Card.Title>
                                <a style={{ color: "#BB2E1F", textDecoration: "none" }} href={allinfo.html_url}><h3> {allinfo.owner.login}</h3></a></Card.Title>
                        </Card.Body>
                    </div>
                    <Card.Body>
                        <Card.Title>
                            <BsFillPersonFill style={{ color: "#FFBF74" }} />
                            <a style={{ color: "black", textDecoration: "none" }} href={`https://github.com/${allinfo.owner.login}`}> &nbsp;&nbsp;<b style={{ color: `${mode === "dark" ? "black" : "white"}` }}>{allinfo.owner.login}</b></a>
                        </Card.Title>
                        <Card.Title style={{ color: `${mode === "dark" ? "black" : "white"}` }}><i className="fa fa-star" style={{ color: "#FFD700" }} aria-hidden="true"></i> &nbsp;&nbsp;{allinfo.stargazers_count} stars</Card.Title>
                        <Card.Title style={{ color: `${mode === "dark" ? "black" : "white"}` }} > <i className="fa fa-code-fork" style={{ color: "#80C3F5" }} aria-hidden="true"></i> &nbsp;&nbsp;{allinfo.forks_count} forks</Card.Title>
                        <Card.Title style={{ color: `${mode === "dark" ? "black" : "white"}` }} ><i className="fa fa-exclamation-triangle" style={{ color: "#EFBBBE" }} aria-hidden="true"></i> &nbsp;&nbsp;{allinfo.open_issues} open issues</Card.Title>
                    </Card.Body>
                    <br />
                </Card>
            )
        })
    }


    return (
        <div className='d-flex justify-content-around flex-wrap overflow-y-scroll ' style={{ height: "fit-content", width: "80%" }}>
            {languageData !== "" && languageData !== undefined && allCards}
        </div>
    )
}
