import React from 'react'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { useDispatch, useSelector } from 'react-redux';
import { dark, light } from '../Mode/ModeSlice';


export default function NavBar() {
    const { mode } = useSelector((store) => store.mode)
    const dispatch = useDispatch()
    return (
        <Navbar style={{ width: "80%", backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}` }} >
            <Container>
                <Navbar.Brand className='d-flex' >
                    <h3 style={{ color: "#BB2E1F" }} className='me-2' >Popular </h3>
                    <h3 style={{ color: `${mode === "dark" ? "black" : "white"}` }}>Battle</h3>
                </Navbar.Brand>
                <Navbar.Brand >
                    {mode === "dark" ? <button onClick={() => dispatch(dark())} style={{ backgroundColor: "white", border: "none" }} >🔦</button> : <button style={{ backgroundColor: "#24282A", border: "none" }} onClick={() => dispatch(light())} >💡</button>}
                </Navbar.Brand>
            </Container>
        </Navbar>
    )
}
