import { configureStore } from '@reduxjs/toolkit'
import languageReducer from './LangageData/LanguageSlice'
import modeReducer from './Components/Mode/ModeSlice'

export const store = configureStore({
    reducer: {
        language: languageReducer,
        mode: modeReducer
    }
})