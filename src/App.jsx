import { useEffect} from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useDispatch, useSelector } from "react-redux"
import { getlanguageData } from './LangageData/LanguageSlice';
import NavBar from './Components/NavBar/NavBar';
import AllLanguage from './Components/AllLanguage/AllLanguage';
import DisplayCard from './Components/DisplayCards/DisplayCard';
import Spinners from './Components/Spinner/Spinner';

function App() {
  const { isLoading, language } = useSelector((store) => store.language)
  const { mode } = useSelector((store) => store.mode)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getlanguageData(language))
  }, [language])

  return (
    <div className="App" style={{ backgroundColor: `${mode === "dark" ? "white" : "#1C2022"}` }} >
      <NavBar />
      <br />
      <AllLanguage />
      <br />
      {isLoading ? <> <h1 style={{ color: `${mode === "dark" ? "#1C2022" : "white"}` }}>Fetching Resp <Spinners /></h1>  </> : < DisplayCard />}

    </div>
  )
}

export default App